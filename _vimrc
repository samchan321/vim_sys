source $VIMRUNTIME/vimrc_example.vim
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set foldmethod=indent
set foldnestmax=10
set number
set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)\ -\ Powered\ By\ Sam 
"% is also an escape for the ( and )
"set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)\ -\ %{v:servername}
"%t = title
"%m = modification state
"just read the documentation
"original form echo expand(:p:h)
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb

set tabstop=4 shiftwidth=4 expandtab
set titlelen=130
inoremap <C-v> <ESC>"*pa
nnoremap <A-v> <C-v>
nnoremap <C-v> "*p
vnoremap <C-v> "*p
vnoremap <C-c> "*y
vnoremap <C-d> "*d
    "data copy with y in visual mode -->  reg 0  and reg ""
    "the data in insert mode is in the  --> reg .
nnoremap x "_x
nnoremap X "_X
nnoremap d "_d
nnoremap D "_D
vnoremap x "_x
vnoremap X "_X
vnoremap d "_d
vnoremap D "_D

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg1 = substitute(arg1, '!', '\!', 'g')
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg2 = substitute(arg2, '!', '\!', 'g')
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let arg3 = substitute(arg3, '!', '\!', 'g')
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      if empty(&shellxquote)
        let l:shxq_sav = ''
        set shellxquote&
      endif
      let cmd = '"' . $VIMRUNTIME . '\diff"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  let cmd = substitute(cmd, '!', '\!', 'g')
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3
  if exists('l:shxq_sav')
    let &shellxquote=l:shxq_sav
  endif
endfunction

